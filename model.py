from fastapi import FastAPI,Query
from pydantic import BaseModel, Field


class Todo(BaseModel):
    title:str
    description:str=Field(None, title="description of the todo")

class Config:
    schema_extra ={"example" : {"title": "Desktop",
    
     "description" : "useful for work"
    
    }
    
    
    
    }